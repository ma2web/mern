import React, { Component } from "react";
import axios from "axios";

class Exercises extends Component {
    constructor(props) {
        super(props);
        this.state = {
            exercises: [],
            loading: true
        };
    }
    componentDidMount() {
        axios.get("http://localhost:5000/exercises/").then(res => {
            this.setState({
                exercises: res.data
            });
        });
    }
    render() {
        return (
            <div>
                {this.state.exercises.length ? (
                    <div className="container p-4">
                        <table class="table">
                            <caption>List of users</caption>
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Username</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Duration</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.exercises.map(exercise => {
                                    return (
                                        <tr>
                                            <th scope="row">{Math.floor(Math.random() * 2)}</th>
                                            <td>{exercise.username}</td>
                                            <td>{exercise.description}</td>
                                            <td>{exercise.duration}</td>
                                            <td>{exercise.date}</td>
                                            <td>
                                                <a href="#">edit</a> {" | "}
                                                <a href="#">delete</a>
                                            </td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>
                ) : (
                    <div className="container p-4">
                        <div className="spinner-border" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default Exercises;
