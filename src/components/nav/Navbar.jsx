import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
    return (
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">
                Navbar
            </a>
            <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarText"
                aria-controls="navbarText"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <Link to="/">
                            <a class="nav-link" href="#">
                                Exercises
                            </a>
                        </Link>
                    </li>
                    <li class="nav-item">
                        <Link to="/create-exercise">
                            <a class="nav-link" href="#">
                                Create Exercise
                            </a>
                        </Link>
                    </li>
                    <li class="nav-item">
                        <Link to="/users">
                            <a class="nav-link" href="#">
                                Users
                            </a>
                        </Link>
                    </li>
                    <li class="nav-item">
                        <Link to="/create-user">
                            <a class="nav-link" href="#">
                                Create User
                            </a>
                        </Link>
                    </li>
                </ul>
                <div class="btn-group navbar-text">
                    <div class="btn-group">
                        <button
                            type="button"
                            class="btn btn-secondary dropdown-toggle"
                            data-toggle="dropdown"
                            data-display="static"
                            aria-haspopup="true"
                            aria-expanded="false"
                        >
                            Profile {" "}
                        </button>
                        <div class="dropdown-menu dropdown-menu-lg-right">
                            <button class="dropdown-item" type="button">
                                Account
                            </button>
                            <button class="dropdown-item" type="button">
                                Logout
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    );
}

export default Navbar;
