import React from "react";
import Navbar from "./components/nav/Navbar";
import { Route, Link, BrowserRouter as Router, Switch } from "react-router-dom";
import Exercises from "./components/exercise/Exercises";
import Users from "./components/user/Users";
import CreateExercise from "./components/exercise/CreateExercise";
import CreateUser from "./components/user/CreateUser";

function App() {
    return (
        <div className="container-fluid">
            <Router>
                <div>
                    <Navbar />
                    <Switch>
                        <Route exact path="/" component={Exercises} />
                        <Route exact path="/create-exercise" component={CreateExercise} />
                        <Route path="/users" component={Users} />
                        <Route path="/create-user" component={CreateUser} />
                    </Switch>
                </div>
            </Router>
        </div>
    );
}

export default App;
