import React, { Component } from "react";

class CreateExercise extends Component {
    render() {
        return (
            <div className="container p-4">
                <form>
                    <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Example label</label>
                        <select class="form-control" id="formGroupExampleInput">
                            <option>Default select</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupExampleInput2">Another label</label>
                        <input
                            type="text"
                            className="form-control"
                            id="formGroupExampleInput2"
                            placeholder="Another input"
                        />
                    </div>
                </form>
            </div>
        );
    }
}

export default CreateExercise;
